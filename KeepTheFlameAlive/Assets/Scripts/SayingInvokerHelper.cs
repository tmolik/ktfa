﻿using UI;
using UnityEngine;

public class SayingInvokerHelper : MonoBehaviour
{
    [SerializeField]
    private Transform _target = null;

    [SerializeField]
    private Vector3 _offset = Vector3.zero;

    [SerializeField]
    private bool _getRandomSaying = false;

    [SerializeField]
    private string _text = "";

    [SerializeField]
    private float _duration = 2f;

    public void ShowSaying()
    {
        if (UiSayingsManager.Instance != null)
            UiSayingsManager.Instance.CreateAndSetupSaying(_target, _offset, _getRandomSaying ? SayingLibrary.Instance.GetRandomSaying(ESayingType.AtTheBeginning) : _text, _duration);
    }
}
