﻿using UnityEngine;

public class FlashlightController : MonoBehaviour
{
    [SerializeField]
    private Light _light = null;

    public void SwitchLight()
    {
        _light.enabled = !_light.enabled;
    }
}
