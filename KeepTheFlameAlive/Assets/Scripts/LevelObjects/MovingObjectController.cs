﻿using System.Collections.Generic;
using UnityEngine;

public class MovingObjectController : MonoBehaviour
{
    [SerializeField]
    private List<Transform> _placesToBe = new List<Transform>();

    [SerializeField]
    private float _timeToSpendAtPosition = 1f;

    [SerializeField]
    private bool _teleportObjectToFirstPositionAtStart = false;

    [SerializeField]
    private float _movementSpeed = 2f;

    private int _destinationPositionIndex = 0;
    private Vector3 _direction = Vector3.zero;
    protected Vector3 _currentVelocity;

    private bool _isAtPosition = false;
    private float _helperTimer = 0;

    public void Start()
    {
        if (_teleportObjectToFirstPositionAtStart && _placesToBe.Count > 0)
            transform.position = _placesToBe[0].position;
    }

    public virtual void Update()
    {
        DetermineToStayOrGo();
       
    }

    private void DetermineToStayOrGo()
    {
        if (_isAtPosition)
        {
            StayAtPosition();
        }
        else
        {
            GoToNextPosition();
        }
    }


    private void StayAtPosition()
    {
        _helperTimer += Time.deltaTime;
        if (_helperTimer >= _timeToSpendAtPosition)
        {
            _isAtPosition = false;
            SetNextPosition();
        }
    }

    private void GoToNextPosition()
    {
        _direction = (_placesToBe[_destinationPositionIndex].position - transform.position).normalized;
        _currentVelocity = _direction * _movementSpeed;
        transform.position += _currentVelocity * Time.deltaTime;
        if (Vector3.Distance(_placesToBe[_destinationPositionIndex].position, transform.position) < 0.2f)
        {
            _isAtPosition = true;
            _helperTimer = 0;
            _currentVelocity = Vector3.zero;
        }
    }
  
    private void SetNextPosition()
    {
        _destinationPositionIndex = _destinationPositionIndex + 1;
        if (_destinationPositionIndex >= _placesToBe.Count)
            _destinationPositionIndex = 0;

    }
}
