﻿using UnityEngine;

public class FlashlightTrap : MonoBehaviour
{
    [SerializeField]
    private MeshCollider _meshCollider=null;

    [SerializeField]
    private CapsuleCollider _capsuleCollider = null;

    private Obstacle _obstacleComponent;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        InitializeComponents();
    }

    private void InitializeComponents()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _obstacleComponent = GetComponent<Obstacle>();
        _obstacleComponent.enabled = false;
    }

    [ContextMenu("Activate")]
    public void ActivateTrap()
    {
        _meshCollider.enabled = false;
        _capsuleCollider.enabled = true;
        _rigidbody.isKinematic = false;
        _rigidbody.useGravity = true;
        _obstacleComponent.enabled = true;
        _rigidbody.AddForce(transform.right * -150, ForceMode.Acceleration);
    }

   
}
