﻿using UnityEngine;
using UnityEngine.Events;

public class TriggerByMatch : MonoBehaviour
{
    [SerializeField]
    private UnityEvent _actionToTrigger = null;

    [SerializeField]
    private bool _activateOnlyOnce = false;

    private bool _activatedOnce = false;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        MatchController matchController = other.attachedRigidbody.GetComponent<MatchController>();
        if (matchController != null && matchController.IsControlledByPlayer)
        {
            ActivateTrigger();
        }
    }

    protected virtual void ActivateTrigger()
    {
        if (_activateOnlyOnce && _activatedOnce)
            return;

        _activatedOnce = true;
        if (_actionToTrigger != null)
            _actionToTrigger.Invoke();
    }
}
