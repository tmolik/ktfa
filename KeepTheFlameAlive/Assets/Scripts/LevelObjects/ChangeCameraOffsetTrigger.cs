﻿using UnityEngine;

public class ChangeCameraOffsetTrigger : MonoBehaviour
{
    [SerializeField]
    private Vector3 _newOffset = Vector3.zero;

    [SerializeField]
    private Vector3 _newRotation = Vector3.zero;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        MatchController matchController = other.attachedRigidbody.GetComponent<MatchController>();
        if (matchController != null && matchController.IsControlledByPlayer)
        {
            FollowingCamera.Instance.SetNewTargetOffsetAndRotation(_newOffset, _newRotation);
        }
    }

    
}
