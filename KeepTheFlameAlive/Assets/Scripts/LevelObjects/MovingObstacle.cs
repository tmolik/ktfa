﻿using UnityEngine;

public class MovingObstacle : MovingObjectController
{
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.attachedRigidbody == null)
            return;

        if (collision.collider.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            matchController.StopTakingControl();
        }
    }
}
