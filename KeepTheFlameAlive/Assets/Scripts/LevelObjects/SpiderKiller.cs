﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderKiller : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if (!enabled)
            return;

        if (collision.collider.attachedRigidbody == null)
            return;

        if (collision.collider.attachedRigidbody.TryGetComponent(out SpiderController spider))
        {
            spider.Kill();
        }
    }
}
