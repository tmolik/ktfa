﻿using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MovingObjectController
{
    private List<MatchController> _matchesOnPlatform = new List<MatchController>();

    public override void Update()
    {
        base.Update();
        AddVelocityToMatchesOnPlatform();
    }

    private void AddVelocityToMatchesOnPlatform()
    {
        if (_matchesOnPlatform.Count <= 0)
            return;

        foreach (MatchController match in _matchesOnPlatform)
        {
            match.AdditionalVelocityFromMovingObjects = _currentVelocity;
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.attachedRigidbody == null)
            return;

        if (collider.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            if (!_matchesOnPlatform.Contains(matchController))
                _matchesOnPlatform.Add(matchController);
        }
    }

    public void OnTriggerExit(Collider collider)
    {
        if (collider.attachedRigidbody == null)
            return;

        if (collider.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            if (_matchesOnPlatform.Contains(matchController))
                _matchesOnPlatform.Remove(matchController);

        }
    }
}
