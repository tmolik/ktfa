﻿using UnityEngine;

public class SteamParticleController : MonoBehaviour
{   
    [SerializeField]
    private float _enabledTime = 3f;

    [SerializeField]
    private float _disabledTime = 2f;

    private ParticleSystem _steamParticle;

    private float _helperTimer = 0;

    public void Awake()
    {
        _steamParticle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        _helperTimer += Time.deltaTime;
        CheckIfShouldChangeSteamState();
    }

    private void CheckIfShouldChangeSteamState()
    {
        if (_steamParticle.isPlaying)
        {
            if (_helperTimer > _enabledTime)
            {
                DisableSteam();
            }
        }
        else
        {
            if (_helperTimer > _disabledTime)
            {
                EnableSteam();
            }
        }
    }

    private void EnableSteam()
    {
        _helperTimer = 0;
        _steamParticle.Play();
    }

    private void DisableSteam()
    {
        _helperTimer = 0;
        _steamParticle.Stop();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.TryGetComponent(out MatchController matchController))
        {
            matchController.Extinguish();
            matchController.AddForceToRigidbody(transform.forward * 1.5f);
        }
    }
}
