﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FuseController : MonoBehaviour
{
    //Define fire speed
    //Define points - trail for fire, it will go in straight line but with many points it will be ok for now
    //Method for starting ignite

    [SerializeField]
    private List<Transform> _trailPoints = new List<Transform>();

    [SerializeField]
    private float _fireSpeed = 2f;

    [SerializeField]
    private Transform _fireTransform = null;

    [SerializeField]
    private ParticleSystem _fireParticle = null;

    [SerializeField]
    private bool _finishLevel = false;

    [SerializeField]
    private float _finishLevelTime = 4f;

    private int _currentTrailPoint;

    private bool _isActive = false;

    public void Ignite()
    {
        _currentTrailPoint = 1;
        _fireTransform.position = _trailPoints[0].position;
        _fireParticle.Play();
        _isActive = true;
        if (_finishLevel)
            StartCoroutine(InvokeFinishLevelAfterTime(_finishLevelTime));
    }

    void Update()
    {
        if (!_isActive)
            return;

        Vector3 direction = (_trailPoints[_currentTrailPoint].position - _fireTransform.position).normalized;
        _fireTransform.position += direction * _fireSpeed * Time.deltaTime;
        if (Vector3.Distance(_fireTransform.position, _trailPoints[_currentTrailPoint].position) < 0.1f)
        {
            SetNextPosition();
        }
    }

    private void SetNextPosition()
    {
        _currentTrailPoint++;
        if (_currentTrailPoint == _trailPoints.Count)
        {
            _isActive = false;
        }
    }

    public void SetAsCameraTargetAndStopMatch()
    {
        if (LevelController.Instance.CurrentMatch != null)
            LevelController.Instance.CurrentMatch.StopTakingControl();
        FollowingCamera.Instance.SetNewTarget(_fireTransform);
    }

    private IEnumerator InvokeFinishLevelAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        LevelController.Instance.FinishedLevel();
    }
}
