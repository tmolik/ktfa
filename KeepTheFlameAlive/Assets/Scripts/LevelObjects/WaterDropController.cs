﻿using ObjectPooling;
using UnityEngine;

public class WaterDropController : MonoBehaviour
{
    private Rigidbody _waterDropRigidbody = null;

    [SerializeField]
    private LayerMask _groundLayerMask = 0;

    private void Awake()
    {
        _waterDropRigidbody = GetComponent<Rigidbody>();
    }

    private void OnDisable()
    {
        _waterDropRigidbody.velocity = Vector3.zero;
    }

    private void Update()
    {
        CheckForHit();
    }

    private void CheckForHit()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.position + Vector3.down, out hit, 2f, _groundLayerMask, QueryTriggerInteraction.Ignore))
        {
            SpawnSplashEffect(hit.point);
            SpawnSound(hit.point);
            gameObject.SetActive(false);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.attachedRigidbody != null)
        {
            if(collision.collider.attachedRigidbody.TryGetComponent(out MatchController matchController))
            {
                matchController.Extinguish();
            }
        }
        SpawnSplashEffect(collision.contacts[0].point);
        SpawnSound(collision.contacts[0].point);
        gameObject.SetActive(false);
    }

    private void SpawnSplashEffect(Vector3 splashPosition)
    {
        GameObject splashGameobject = ObjectPooler.Get.GetPooledObject(EPoolables.WaterDropSplash);
        if (splashGameobject != null)
        {
            splashGameobject.transform.position = splashPosition;
            splashGameobject.SetActive(true);
        }
    }

    private void SpawnSound(Vector3 soundPosition)
    {
        AudioSource audioSource = ObjectPooler.Get.GetPooledObject(EPoolables.WaterDropSound).GetComponent<AudioSource>();
        audioSource.clip = AudioClipsHelper.Instance.GetRandomWaterDropClip();
        audioSource.transform.position = soundPosition;
        audioSource.spatialBlend = 1f;
        audioSource.gameObject.SetActive(true);
        audioSource.Play();
    }
}
