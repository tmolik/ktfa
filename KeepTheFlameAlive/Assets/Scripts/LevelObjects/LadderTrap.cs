﻿using UnityEngine;

public class LadderTrap : MonoBehaviour
{
    [SerializeField]
    private Obstacle _obstacleComponent=null;

    public void DisableObstacle()
    {
        Destroy(_obstacleComponent);
    }
}
