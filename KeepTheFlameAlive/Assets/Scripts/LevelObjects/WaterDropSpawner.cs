﻿using ObjectPooling;
using UnityEngine;

public class WaterDropSpawner : MonoBehaviour
{
    [SerializeField]
    private float _spawnInterval = 2f;

    private float _helperTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        _helperTimer = Random.Range(-0.6f,0f);
    }

    // Update is called once per frame
    void Update()
    {
        _helperTimer += Time.deltaTime;
        CheckIfShouldSpawnDrop();
    }

    private void CheckIfShouldSpawnDrop()
    {
        if (_helperTimer >= _spawnInterval)
        {
            SpawnDrop();
        }
    }

    private void SpawnDrop()
    {
        GameObject drop = ObjectPooler.Get.GetPooledObject(EPoolables.WaterDrop);
        drop.transform.position = transform.position;
        drop.SetActive(true);

        _helperTimer = 0;
    }
}
