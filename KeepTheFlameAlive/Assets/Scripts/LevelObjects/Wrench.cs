﻿using UnityEngine;

public class Wrench : Obstacle
{
    private Vector3 _velocity = Vector3.zero;

    private Vector3 _lastFramePosition;

    // Update is called once per frame
    void Update()
    {
        _velocity = _lastFramePosition - transform.position;
        _lastFramePosition = transform.position;
    }

    protected override void AdditionalOnCollisionEnter(MatchController match)
    {
        match.AddForceToRigidbody(_velocity * -150);
    }
}
