﻿using UnityEngine;

public class Water : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            matchController.Extinguish();
        }
    }
}
