﻿using System.Collections;
using UnityEngine;

public class TimedTrigger : TriggerByMatch
{
    [SerializeField]
    private float _activationTime = 2f;

    protected override void ActivateTrigger()
    {
        StartCoroutine(ActivateAfterTime());
    }

    private IEnumerator ActivateAfterTime()
    {
        yield return new WaitForSeconds(_activationTime);
        base.ActivateTrigger();
    }
}
