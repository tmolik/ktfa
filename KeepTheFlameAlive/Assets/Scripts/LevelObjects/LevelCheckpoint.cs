﻿using UnityEngine;

public class LevelCheckpoint : MonoBehaviour
{
    public int CheckPointID = 0;

    [SerializeField]
    private Transform _matchStartPoint = null;

    [SerializeField]
    private ParticleSystem _fireController = null;

    [SerializeField]
    private Light _fireLight = null;

    private bool _visited = false;

    public void SetCheckpointAsVisited()
    {
        if (_visited)
            return;

        _visited = true;
        CheckpointsController.Instance.SetCheckpointAsVisited(this);
        _fireController.Play();
        _fireLight.gameObject.SetActive(true);
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;
        MatchController match = other.attachedRigidbody.GetComponent<MatchController>();
        if (match != null && match.IsControlledByPlayer)
        {
            SetCheckpointAsVisited();
        }
    }

    public Vector3 GetMatchStartPosition()
    {
        return _matchStartPoint != null ? _matchStartPoint.position : transform.position;
    }

    public void OnValidate()
    {
        gameObject.name = "Checkpoint" + CheckPointID;
    }
}
