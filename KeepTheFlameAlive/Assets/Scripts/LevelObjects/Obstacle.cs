﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if (!enabled)
            return;

        if (collision.collider.attachedRigidbody == null)
            return;

        if (collision.collider.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            matchController.StopTakingControl();
            matchController.CanBeControlledByPlayer = false;
            AdditionalOnCollisionEnter(matchController);
        }
    }

    protected virtual void AdditionalOnCollisionEnter(MatchController match)
    {
    }
}
