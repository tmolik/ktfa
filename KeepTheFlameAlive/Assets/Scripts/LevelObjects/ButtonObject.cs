﻿using UnityEngine;
using UnityEngine.Events;

public class ButtonObject : MonoBehaviour
{
    private const string BUTTON_ACTIVATED_BOOL = "Activated";

    [SerializeField]
    private Animator _animator = null;

    [SerializeField]
    private bool _deactivateOnTriggerExit = false;

    [SerializeField]
    private UnityEvent _onActivateEvent=null;

    [SerializeField]
    private UnityEvent _onDeactivateEvent = null;

    private bool _isActivated = false;

    private void Activate()
    {
        _isActivated = true;
        _onActivateEvent.Invoke();
        _animator.SetBool(BUTTON_ACTIVATED_BOOL, true);
    }

    private void Deactivate()
    {
        _isActivated = false;
        _onDeactivateEvent.Invoke();
        _animator.SetBool(BUTTON_ACTIVATED_BOOL, false);
    }


    public void OnTriggerEnter(Collider collider)
    {
        if (!_isActivated)
            Activate();
    }

    public void OnTriggerExit(Collider collider)
    {
        if (_deactivateOnTriggerExit && _isActivated)
            Deactivate();
    }
}
