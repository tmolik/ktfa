﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsObstacle : Obstacle
{
    [SerializeField]
    private Vector3 _forceToAdd = Vector3.zero;

    [SerializeField]
    private ForceMode _forceModeToAdd;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        _rigidbody.AddForce(_forceToAdd, _forceModeToAdd);
    }
}
