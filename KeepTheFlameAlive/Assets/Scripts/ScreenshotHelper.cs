﻿using UnityEngine;

public class ScreenshotHelper : MonoBehaviour
{
    private int index = 0;

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ScreenCapture.CaptureScreenshot("D://Screenshot" + index + ".png");
            index += 1;
        }
    }
}
