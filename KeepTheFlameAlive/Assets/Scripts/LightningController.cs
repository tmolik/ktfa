﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningController : MonoBehaviour
{
    [SerializeField]
    private List<string> _animationNames = new List<string>();

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(WaitRandomTimeAndPlayAnimation());
    }

    private IEnumerator WaitRandomTimeAndPlayAnimation()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(10, 25));
            _animator.Play(_animationNames.GetRandom());
            StartCoroutine(CreateAudioEffect());
        }
    }

    private IEnumerator CreateAudioEffect()
    {
        yield return new WaitForSeconds(Random.Range(1, 6));
        AudioManager.Instance.PlayAudio2D(AudioClipsHelper.Instance.GetRandomLightingClip());
    }
}
