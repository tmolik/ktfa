﻿using System.Collections.Generic;
using UnityEngine;

public class AudioClipsHelper : Singleton<AudioClipsHelper>
{
    [SerializeField]
    private List<AudioClip> _waterDropsClips = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> _lightingsClips = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> _mosquitoDeathClips = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> _spiderDeathClips = new List<AudioClip>();

    public AudioClip GetRandomWaterDropClip()
    {
        return _waterDropsClips.GetRandom();
    }

    public AudioClip GetRandomLightingClip()
    {
        return _lightingsClips.GetRandom();
    }

    public AudioClip GetRandomMosquitoDeathClip()
    {
        return _mosquitoDeathClips.GetRandom();
    }
    public AudioClip GetRandomSpiderDeathClip()
    {
        return _spiderDeathClips.GetRandom();
    }
}
