﻿using UnityEngine;

public class PlaySoundSignalReceiver : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audioSource = null;

    public void PlaySound()
    {
        _audioSource.Play();
    }
}
