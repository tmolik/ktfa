﻿using UnityEngine;

public class LoadLastLevelHelper : MonoBehaviour
{
    public void LoadLastLevelHardcoded()
    {
        LevelsManager.Instance.LoadLevelWithId(3);
    }
}
