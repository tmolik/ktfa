using System.Collections.Generic;
using UnityEngine;

public class DisableOnPlatform : MonoBehaviour
{
    [SerializeField]
    private List<RuntimePlatform> _runtimePlatformsToDisableOn = new List<RuntimePlatform>();

    public void Awake()
    {
        if(_runtimePlatformsToDisableOn.Contains(Application.platform))
        {
            gameObject.SetActive(false);
        }
    }
}
