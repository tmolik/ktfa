﻿using Molioo.Localization;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UiSayingController : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _sayingText = null;

        [SerializeField]
        private float _scalingDuration = 0.25f;

        private Vector3 _offset = Vector3.zero;
        private Vector3 _correctPosition;

        private Transform _targetTransform = null;
        private RectTransform _rectTransform = null;

        private float _sayingDuration = 0;

        private Camera _camera;

        private bool _scalingUp = false;
        private bool _scalingDown = false;
        private float _scalingTimer = 0;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }


        public void SetupSaying(Camera camera, Transform target, Vector3 offset, string text, float duration)
        {
            _sayingDuration = duration;
            _targetTransform = target;
            _offset = offset;
            _sayingText.text = LocalizationManager.Instance.GetLocalizedText(text);
            _camera = camera;

            _scalingUp = true;
            _scalingTimer = 0;
            _rectTransform.localScale = Vector3.zero;
            _correctPosition = _camera.WorldToScreenPoint(_targetTransform.position + _offset);
            _rectTransform.position = _correctPosition;
        }

        private void Update()
        {
            if (_scalingUp)
            {
                ScaleUpUpdate();
            }
            else if (_scalingDown)
            {
                ScaleDownUpdate();
            }
            else
            {
                DurationCheckUpdate();
            }

            if (_camera != null)
            {
                _correctPosition = Vector3.Lerp(_rectTransform.position, _camera.WorldToScreenPoint(_targetTransform.position + _offset), Time.deltaTime * 4);
                _rectTransform.position = _correctPosition;
            }
        }

        private void ScaleUpUpdate()
        {
            _scalingTimer += Time.deltaTime;
            _rectTransform.localScale = Vector3.one * Mathf.Lerp(0, 1, _scalingTimer / _scalingDuration);
            if (_scalingTimer >= _scalingDuration)
            {
                _scalingUp = false;
            }
        }

        private void ScaleDownUpdate()
        {
            _scalingTimer += Time.deltaTime;
            _rectTransform.localScale = Vector3.one * Mathf.Lerp(1, 0, _scalingTimer / _scalingDuration);
            if (_scalingTimer >= _scalingDuration)
            {
                _scalingDown = false;
                gameObject.SetActive(false);
            }
        }

        private void DurationCheckUpdate()
        {
            _sayingDuration -= Time.deltaTime;
            if (_sayingDuration <= 0)
            {
                _scalingDown = true;
                _scalingTimer = 0;
            }
        }
    }
}