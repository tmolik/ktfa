﻿using ObjectPooling;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UiSayingsManager : Singleton<UiSayingsManager>
    {
        private Camera _camera;

        public override void Awake()
        {
            base.Awake();

            if (FollowingCamera.Instance != null)
                _camera = FollowingCamera.Instance.GetComponent<Camera>();
        }

        public void CreateAndSetupSaying(Transform target, Vector3 offset, string text, float duration)
        {
            if (_camera == null && FollowingCamera.Instance != null)
                _camera = FollowingCamera.Instance.GetComponent<Camera>();

            GameObject sayingGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.UiSaying);
            if (sayingGameObject != null)
            {
                UiSayingController sayingController = sayingGameObject.GetComponent<UiSayingController>();
                sayingController.SetupSaying(_camera, target, offset, text, duration);
                sayingController.transform.SetParent(transform);
                sayingController.gameObject.SetActive(true);
            }
        }

        public void CreateAndSetupIntroSaying(Camera introCamera, Transform target, Vector3 offset, string text, float duration)
        {
            GameObject sayingGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.UiSaying);
            if (sayingGameObject != null)
            {
                UiSayingController sayingController = sayingGameObject.GetComponent<UiSayingController>();
                sayingController.SetupSaying(introCamera, target, offset, text, duration);
                sayingController.transform.SetParent(transform);
                sayingController.gameObject.SetActive(true);
            }
        }

    }
}