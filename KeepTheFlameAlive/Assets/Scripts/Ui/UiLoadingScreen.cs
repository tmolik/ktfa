﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UiLoadingScreen : Singleton<UiLoadingScreen>
    {
        [SerializeField]
        private Image _progressImage = null;

        private CanvasGroup _canvasGroup = null;

        public override void Awake()
        {
            base.Awake();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Show()
        {
            StopAllCoroutines();
            StartCoroutine(ShowLoadingScreen());
        }

        public void Hide()
        {
            StopAllCoroutines();
            StartCoroutine(HideLoadingScreen());
        }

        public void UpdateProgress(float progress)
        {
            _progressImage.fillAmount = progress;
        }

        private IEnumerator ShowLoadingScreen(float fadeTime = 0.3f)
        {
            _progressImage.fillAmount = 0;
            float timer = 0.0f;
            while (true)
            {
                timer += Time.deltaTime;
                _canvasGroup.alpha = Mathf.Lerp(0, 1, timer / fadeTime);
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator HideLoadingScreen(float fadeTime = 0.3f)
        {
            float timer = 0.0f;
            while (true)
            {
                timer += Time.deltaTime;
                _canvasGroup.alpha = Mathf.Lerp(1, 0, timer / fadeTime);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}