﻿using UnityEngine;

namespace UI
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _menuButtonsPanel = null;

        [SerializeField]
        private RectTransform _settingsPanel = null;

        [SerializeField]
        private RectTransform _continueButtonRect = null;

        private void Start()
        {
            _continueButtonRect.gameObject.SetActive(GameFilesManager.CurrentGameData.CurrentLevel != -1);
            CursorManager.Instance.ShowCursor();
        }


        public void NewGame()
        {
            CheckpointsController.Instance.ResetCheckpoint();
            LevelsManager.Instance.LoadFirstLevel();
        }

        public void Continue()
        {
            Debug.LogError("Load level with id " + GameFilesManager.CurrentGameData.CurrentLevel);
            LevelsManager.Instance.LoadLevelWithId(GameFilesManager.CurrentGameData.CurrentLevel);
        }

        public void Settings()
        {
            _settingsPanel.gameObject.SetActive(true);
            _menuButtonsPanel.gameObject.SetActive(false);
        }

        public void GoBackToMenuButtons()
        {
            _settingsPanel.gameObject.SetActive(false);
            _menuButtonsPanel.gameObject.SetActive(true);
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}