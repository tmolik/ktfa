﻿using Molioo.Localization;
using TMPro;
using UnityEngine;

public class LanguageButton : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _languageText=null;

    private void Awake()
    {
        LocalizationManager.LanguageChanged += SetLanguage;
        SetLanguage(LocalizationManager.Instance.GetLanguage());
    }

    private void SetLanguage(ELanguage language)
    {
        _languageText.text = language.ToString();
        SettingsManager.CurrentSettings.Language = language;
    }

    public void OnClick()
    {
        LocalizationManager.Instance.SetNextLanguage();
    }
}
