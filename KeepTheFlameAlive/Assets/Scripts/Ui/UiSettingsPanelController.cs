﻿using UI;
using UnityEngine;
using UnityEngine.UI;

public class UiSettingsPanelController : MonoBehaviour
{
    [SerializeField]
    private MenuController _menuController = null;

    [SerializeField]
    private UiPauseMenu _pauseMenuController = null;

    [SerializeField]
    private Slider _masterVolumeSlider = null;

    [SerializeField]
    private Slider _musicVolumeSlider = null;

    [SerializeField]
    private Slider _brightnessSlider = null;

    public void OnClickBack()
    {
        if (_menuController)
            _menuController.GoBackToMenuButtons();

        if (_pauseMenuController)
            _pauseMenuController.GoBackFromSettings();
        SaveSettings();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnClickBack();
        }
    }

    public void OnEnable()
    {
        SetupFromSettings();
    }

    public void SetupFromSettings()
    {
        _masterVolumeSlider.value = SettingsManager.CurrentSettings.MasterVolume;
        _musicVolumeSlider.value = SettingsManager.CurrentSettings.MusicVolume;
        _brightnessSlider.value = SettingsManager.CurrentSettings.Brightness;
    }

    public void ChangedMasterVolume(float volume)
    {
        SettingsManager.CurrentSettings.MasterVolume = volume;
        AudioManager.Instance.SetMasterVolume();
    }

    public void ChangedMusicVolume(float volume)
    {
        SettingsManager.CurrentSettings.MusicVolume = volume;
        AudioManager.Instance.SetMusicVolume();
    }

    public void ChangedBrightness(float volume)
    {
        SettingsManager.CurrentSettings.Brightness = volume;
        GraphicsSettingsController.Instance.SetupBrightness();
    }

    public void SaveSettings()
    {
        SettingsManager.SaveSettings();
    }
}
