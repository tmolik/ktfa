﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UiPauseMenu : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup _pauseCanvasGroup = null;

        [SerializeField]
        private RectTransform _buttonsRect = null;
        [SerializeField]
        private RectTransform _settingsRect = null;

        private bool _isShown = false;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_isShown)
                    Hide();
                else
                    Show();
            }
        }

        public void OnEnable()
        {
            _buttonsRect.gameObject.SetActive(true);
            _settingsRect.gameObject.SetActive(false);
        }


        public void Show()
        {
            if (!LevelsManager.Instance.CanShowPauseMenu)
                return;

            _pauseCanvasGroup.SwitchAllValues(true);
            Time.timeScale = 0;
            _isShown = true;
            CursorManager.Instance.ShowCursor();
        }

        public void Hide()
        {
            _pauseCanvasGroup.SwitchAllValues(false);
            Time.timeScale = 1;
            _isShown = false;
            CursorManager.Instance.HideCursor();
        }

        public void ClickedContinue()
        {
            Hide();
        }

        public void ClickedExitToMenu()
        {
            LevelsManager.Instance.GoBackToMenu();
            UiLoseMenu.Instance.Hide();
            Hide();
        }

        public void ClickedSettings()
        {
            _buttonsRect.gameObject.SetActive(false);
            _settingsRect.gameObject.SetActive(true);
        }

        public void GoBackFromSettings()
        {
            _buttonsRect.gameObject.SetActive(true);
            _settingsRect.gameObject.SetActive(false);
        }
    }
}