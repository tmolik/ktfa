﻿using UnityEngine;

namespace UI
{
    public class UiLoseMenu : Singleton<UiLoseMenu>
    {
        [SerializeField]
        private CanvasGroup _loseCanvasGroup = null;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                ClickedRestartKey();
            }
        }

        public void Show()
        {
            _loseCanvasGroup.SwitchAllValues(true);
        }

        public void Hide()
        {
            _loseCanvasGroup.SwitchAllValues(false);
        }

        public void ClickedRestartKey()
        {
            Hide();
            LevelController.Instance.RestartLevel();
        }
    }
}
