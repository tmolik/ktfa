﻿using UnityEngine;

public class GameplayInitializator : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod(loadType: RuntimeInitializeLoadType.AfterSceneLoad)]
   public static void SpawnEssentialObjects()
    {
        GameObject _essentialObjectsParent = GameObject.Instantiate(Resources.Load("EssentialObjects") as GameObject);
        DontDestroyOnLoad(_essentialObjectsParent);
    }
}
