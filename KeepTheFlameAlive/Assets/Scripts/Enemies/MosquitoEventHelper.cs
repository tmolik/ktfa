﻿using UnityEngine;

public class MosquitoEventHelper : MonoBehaviour
{
    [SerializeField]
    private MosquitoController _mosquitoController=null;

    public void OnAttackPeek()
    {
        _mosquitoController.OnAttackPeek();
    }
   
}
