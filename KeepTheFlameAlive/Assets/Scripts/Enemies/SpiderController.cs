﻿using FIMSpace.GroundFitter;
using UnityEngine;
using UnityEngine.AI;

public class SpiderController : MonoBehaviour
{
    [SerializeField]
    private FGroundFitter_Base _targetGroundFitter = null;

    [SerializeField]
    private Animator _spiderAnimator = null;

    [SerializeField]
    private Transform _leftLegEndTransform = null;
    [SerializeField]
    private Transform _rightLegEndTransform = null;

    [SerializeField]
    private AudioSource _deathAudioSource = null;
    [SerializeField]
    private AudioSource _walkAudioSource = null;

    private MatchController _targetMatch = null;

    private bool _isChasing = false;
    private bool _isAlive = true;

    private NavMeshAgent _agent;

    private Vector3 _targetPosition;
    private Vector3 _targetDirection;

    private float _attackTimer = 0;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _isChasing = true;
        _isAlive = true;
    }

    public void StartChasingMatch()
    {
        _isChasing = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_isAlive)
            return;

        _attackTimer -= Time.deltaTime;
        DetermineToChaseOrStay();
    }

    private void DetermineToChaseOrStay()
    {
        _isChasing = LevelController.Instance.CurrentMatch != null && LevelController.Instance.CurrentMatch.IsControlledByPlayer;
        if (_isChasing)
        {
            ChasePlayer();
        }
        else
        {
            StayAtCurrentPosition();
        }
    }

    private void ChasePlayer()
    {
        _targetMatch = LevelController.Instance.CurrentMatch;
        _targetDirection = (_targetMatch.transform.position - transform.position).normalized;
        _targetPosition = _targetMatch.transform.position - _targetDirection * 0.75f;
        _agent.SetDestination(_targetPosition);
        _spiderAnimator.SetFloat("Speed", _agent.velocity.magnitude);

        if (Vector3.Distance(transform.position, _targetPosition) < 1.1f && _attackTimer <= 0)
        {
            _spiderAnimator.SetTrigger("Attack");
            _attackTimer = 1f;
        }

        // Calculating look rotation to target point
        Vector3 targetPoint = _agent.nextPosition + _agent.desiredVelocity;
        float yRotation = Quaternion.LookRotation(new Vector3(targetPoint.x, 0f, targetPoint.z) - transform.position).eulerAngles.y;

        // Setting top down rotation in ground fitter component with smoothing (lerp)
        _targetGroundFitter.UpAxisRotation = Mathf.LerpAngle(_targetGroundFitter.UpAxisRotation, yRotation, Time.deltaTime * 4);
    }

    private void StayAtCurrentPosition()
    {
        _agent.SetDestination(transform.position);
        _spiderAnimator.SetFloat("Speed", _agent.velocity.magnitude);
    }

    public void AttackPeak()
    {
        Vector3 centerPosition = (_leftLegEndTransform.position + _rightLegEndTransform.position) / 2f;
        if (Vector3.Distance(transform.position, _targetPosition) <0.7f)
        {
            _targetMatch.StopTakingControl();
            Vector3 diffVector = ((_targetPosition + Vector3.up*0.2f) - transform.position).normalized;
            if (_targetMatch != null)
                _targetMatch.AddForceToRigidbodyAtPosition(diffVector * 10f, centerPosition);
        }
    }

    public void Kill()
    {
        _spiderAnimator.Play("Death");
        _agent.enabled = false;
        _targetGroundFitter.enabled = false;
        _deathAudioSource.clip = AudioClipsHelper.Instance.GetRandomSpiderDeathClip();
        _deathAudioSource.Play();
        _walkAudioSource.Stop();
        _isAlive = false;
    }
}
