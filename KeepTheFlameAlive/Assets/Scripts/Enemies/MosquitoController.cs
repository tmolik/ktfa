﻿using UnityEngine;

public class MosquitoController : MonoBehaviour
{
    [SerializeField]
    private Animator _mosquitoAnimator = null;

    [SerializeField]
    private BoxCollider _mosquitoCollider = null;

    [SerializeField]
    private AudioSource _flyingAudioSource = null;

    [SerializeField]
    private AudioSource _deathAudioSource = null;

    [SerializeField]
    private float _flySpeed = 3;

    //Have a target reference
    //Fly to be always on target Z position
    //When target is close - attack
    //When match jumps on it, it dies
    private MatchController _targetMatch = null;

    private bool _isMoving = false;

    private bool _isAlive = true;

    private void Update()
    {
        if (!_isAlive)
            return;

        SetCorrectPositionAndRotation();
    }

    private void SetCorrectPositionAndRotation()
    {
        _isMoving = LevelController.Instance.CurrentMatch != null && LevelController.Instance.CurrentMatch.IsControlledByPlayer;
        if (_isMoving)
        {
            _targetMatch = LevelController.Instance.CurrentMatch;

            SetCorrectZPosition();
            SetCorrectRotation();
        }
    }

    private void SetCorrectZPosition()
    {
        Vector3 desiredPosition = new Vector3(transform.position.x, transform.position.y, _targetMatch.transform.position.z);
        transform.position += (desiredPosition - transform.position).normalized * _flySpeed * Time.deltaTime;
    }

    private void SetCorrectRotation()
    {
        Vector3 lookDirection = (_targetMatch.transform.position - transform.position);
        lookDirection.y = 0;
        lookDirection.Normalize();
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDirection), Time.deltaTime * 4);
    }

    private void Die()
    {
        _flyingAudioSource.Stop();
        _deathAudioSource.clip = AudioClipsHelper.Instance.GetRandomMosquitoDeathClip();
        _deathAudioSource.Play();
        _mosquitoAnimator.Play("Death");
        _isAlive = false;
        _mosquitoCollider.enabled = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.attachedRigidbody == null)
            return;

        if(collision.collider.attachedRigidbody.TryGetComponent(out MatchController match))
        {
            Vector3 upCheckVector = ((match.transform.position) - transform.position).normalized;
            Vector3 forwardCheckVector = ((match.transform.position + Vector3.up * 0.8f) - transform.position).normalized;
            float angleBetweenForward = Vector3.Angle(forwardCheckVector, transform.forward);
            float angleBetweenUp = Vector3.Angle(upCheckVector, transform.up);
            if (Mathf.Abs(angleBetweenForward) < 80)
            {
                _mosquitoAnimator.Play("Attack");
                match.StopTakingControl();
            }
            else if (Mathf.Abs(angleBetweenUp) < 15)
            {
                Die();
            }
        }
    }

    public void OnAttackPeek()
    {
        _targetMatch.StopTakingControl();
        Vector3 diffVector = ((_targetMatch.transform.position + Vector3.up * 0.4f) - transform.position).normalized;
        if (_targetMatch != null)
            _targetMatch.AddForceToRigidbodyAtPosition(diffVector * 10f, transform.position);
    }
}
