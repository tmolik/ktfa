﻿using UnityEngine;

public class StartupController : MonoBehaviour
{
    private void Start()
    {
        SettingsManager.Initialize();
        QualitySettings.SetQualityLevel(5);
        GameFilesManager.LoadProgress();
        CheckpointsController.Instance.SetupCheckpointFromGameFiles();
        if (LevelsManager.Instance != null)
            LevelsManager.Instance.LoadMenuFirstTime("Level_StartScene");
    }

}
