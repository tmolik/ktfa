﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : Singleton<LevelController>
{
    public override void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }


    [System.NonSerialized]
    public MatchController CurrentMatch = null;

    [SerializeField]
    private MatchController _startingMatch = null;

    [SerializeField]
    private List<LevelCheckpoint> _levelCheckpoints = new List<LevelCheckpoint>();

    [SerializeField]
    private bool _debugSetCheckpoint = false;

    [SerializeField]
    private int _debugCheckpoint = 0;

    private float _canChangeCheckpointTime = 0;

    private void Start()
    {
        GatherCheckpoints();

        int checkpointId = _debugSetCheckpoint ? _debugCheckpoint : CheckpointsController.Instance.GetCurrentCheckpointId();
        if (checkpointId < 0)
            return;

        FindCheckpointAndSetMatchPosition(checkpointId,_startingMatch);
        FollowingCamera.Instance.GoToTargetInstantly();

        UiLoseMenu.Instance.Hide();
    }

    private void Update()
    {
        HandleDebugInput();
    }

    private void HandleDebugInput()
    {
        if (Debug.isDebugBuild && Input.GetKeyDown(KeyCode.T))
        {
            FinishedLevel();
        }
        _canChangeCheckpointTime -= Time.deltaTime;
        if (Input.GetKey(KeyCode.M) && Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.L) && _canChangeCheckpointTime <= 0)
        {
            SkipToNextCheckpoint();
        }
    }

    private void SkipToNextCheckpoint()
    {
        bool foundCheckpoint = FindCheckpointAndSetMatchPosition(CheckpointsController.Instance.GetCurrentCheckpointId() + 1, LevelController.Instance.CurrentMatch);
        _canChangeCheckpointTime = 1f;
        if (!foundCheckpoint)
            FinishedLevel();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void FinishedLevel()
    {
        CheckpointsController.Instance.ResetCheckpoint();
        LevelsManager.Instance.LoadNextLevel();
    }

    private bool FindCheckpointAndSetMatchPosition(int checkpointID, MatchController match)
    {
        for (int i = 0; i < _levelCheckpoints.Count; i++)
        {
            if (_levelCheckpoints[i].CheckPointID == checkpointID)
            {
                match.transform.position = _levelCheckpoints[i].GetMatchStartPosition();
                _levelCheckpoints[i].SetCheckpointAsVisited();
                return true;
            }
        }

        return false;
    }

    public void CheckIfShouldEndGame()
    {
        StartCoroutine(WaitSecondAndCheck());
    }

    private IEnumerator WaitSecondAndCheck()
    {
        yield return new WaitForSeconds(1);

        if (CurrentMatch == null)
            UiLoseMenu.Instance.Show();
    }

    [ContextMenu("Gather and sort checkpoints")]
    public void GatherCheckpoints()
    {
        _levelCheckpoints.Clear();
        LevelCheckpoint[] _checkpoints = GameObject.FindObjectsOfType<LevelCheckpoint>();
        _checkpoints = _checkpoints.OrderBy(check => check.CheckPointID).ToArray();
        _levelCheckpoints.AddRange(_checkpoints);
    }

}
