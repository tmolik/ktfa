﻿public class CheckpointsController : Singleton<CheckpointsController>
{
    private int _currentCheckpointID = -1;

    public void SetupCheckpointFromGameFiles()
    {
        _currentCheckpointID = GameFilesManager.CurrentGameData.CurrentCheckpoint;
    }

    public void SetCheckpointAsVisited(LevelCheckpoint checkpoint)
    {
        _currentCheckpointID = checkpoint.CheckPointID;
        if (GameFilesManager.CurrentGameData != null)
        {
            GameFilesManager.CurrentGameData.CurrentCheckpoint = checkpoint.CheckPointID;
            GameFilesManager.SaveProgress();
        }
    }

    public void ResetCheckpoint()
    {
        _currentCheckpointID = -1;
    }

    public int GetCurrentCheckpointId()
    {
        return _currentCheckpointID;
    }
}
