﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "LevelPreset", menuName = "ScriptableObjects/LevelPreset")]
public class Level : ScriptableObject
{
    public int Level_ID;
    public string Level_Name;
}
