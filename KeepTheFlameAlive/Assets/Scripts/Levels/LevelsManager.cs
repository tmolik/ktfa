﻿using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsManager : Singleton<LevelsManager>
{
    public bool CanShowPauseMenu = false;

    public List<Level> PlayableLevelsList = new List<Level>();

    [SerializeField]
    private Level _menuLevel = null;

    private Level _currentLevel = null;
    private string _currentSceneName = "";

    private bool _isLoading = false;

    internal void LoadNextLevel()
    {
        if (_currentLevel == null)
            return;

        int indexOfCurrentLevel = PlayableLevelsList.IndexOf(_currentLevel);
        if (indexOfCurrentLevel >= PlayableLevelsList.Count-1)
        {
            Debug.LogError("To był ostatni poziom, jaki next level ziomek co ty");
            return;
        }

        Level nextLevel = PlayableLevelsList[indexOfCurrentLevel + 1];
        if (nextLevel != null)
        {
            LoadLevelWithId(nextLevel.Level_ID);
        }
    }

    public void LoadFirstLevel()
    {
        LoadLevelWithId(0);
    }

    public void LoadMenuFirstTime(string startupSceneName)
    {
        _currentLevel = null;
        StartCoroutine(LoadUnloadScenes(_menuLevel.Level_Name, startupSceneName));
    }

    public void GoBackToMenu()
    {
        StartCoroutine(LoadUnloadScenes(_menuLevel.Level_Name, _currentSceneName));
    }


    public void LoadLevelWithId(int id)
    {
        foreach (Level level in PlayableLevelsList)
        {
            if (level.Level_ID == id)
            {
                _currentLevel = level;
                StartCoroutine(LoadUnloadScenes(level.Level_Name, _currentSceneName, true));
                CursorManager.Instance.HideCursor();
                GameFilesManager.CurrentGameData.CurrentLevel = level.Level_ID;
                GameFilesManager.SaveProgress();
                break;
            }
        }
    }

    private IEnumerator LoadUnloadScenes(string sceneToLoad, string sceneToUnload, bool enablePauseMenu = false)
    {
        if (_isLoading)
            yield break;

        _isLoading = true;
        UiLoadingScreen.Instance.Show();
        yield return new WaitForSeconds(.3f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

        while (!asyncLoad.isDone)
        {
            UiLoadingScreen.Instance.UpdateProgress(asyncLoad.progress / 2f);
            yield return null;
        }
        yield return new WaitForSeconds(.3f);
        _currentSceneName = sceneToLoad;
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneToLoad));

        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(sceneToUnload);
        while (!asyncUnload.isDone)
        {
            UiLoadingScreen.Instance.UpdateProgress(0.5f + asyncUnload.progress / 2f);
            yield return null;
        }
        UiLoadingScreen.Instance.UpdateProgress(1);

        UiLoadingScreen.Instance.Hide();
        _isLoading = false;
        CanShowPauseMenu = enablePauseMenu;
    }
}
