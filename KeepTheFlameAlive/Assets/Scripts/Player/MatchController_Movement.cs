﻿using System.Collections;
using UnityEngine;

public partial class MatchController
{
    [SerializeField]
    private MobileInputVariable _mobileInputVariable;

    // Update is called once per frame
    void Update()
    {
        CheckJumpInput();
    }

    public void FixedUpdate()
    {
        if (!CanBeControlledByPlayer)
            return;

        if (LevelController.Instance.CurrentMatch != null && Vector3.Distance(transform.position, LevelController.Instance.CurrentMatch.transform.position) > 20f)
            return;

        _receiveInput = IsControlledByPlayer && _fireController.IsActive;

        float horizontal = 0;
        float vertical = 0;
        if (_receiveInput)
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");

#if UNITY_ANDROID && !UNITY_EDITOR
        horizontal = _mobileInputVariable.MovementVector.x;
        vertical = _mobileInputVariable.MovementVector.y;
#endif

        }
        GroundRaycast();

        if (_isGrounded)
        {
            if (_receiveInput)
            {
                if (AdditionalVelocityFromMovingObjects != Vector3.zero)
                    _matchRigidbody.velocity = new Vector3(horizontal * _movementSpeed, 0, vertical * _movementSpeed) + AdditionalVelocityFromMovingObjects;
                else
                    _matchRigidbody.velocity = new Vector3(horizontal * _movementSpeed, _matchRigidbody.velocity.y, vertical * _movementSpeed);
            }
            else
            {
                if (AdditionalVelocityFromMovingObjects != Vector3.zero)
                    _matchRigidbody.velocity = AdditionalVelocityFromMovingObjects;
            }
        }
        else
        {
            if (_receiveInput)
            {
                _matchRigidbody.velocity = new Vector3(horizontal * _movementSpeed, _matchRigidbody.velocity.y, vertical * _movementSpeed);
            }
        }

        ClampYVelocity();
    }

    public void Jump()
    {
        if (_isInAir)
            _secondJumpPossible = false;
        _isInAir = true;
        AdditionalVelocityFromMovingObjects = Vector3.zero;
        if (_matchRigidbody.velocity.y < 0)
            _matchRigidbody.velocity = new Vector3(_matchRigidbody.velocity.x, 0, _matchRigidbody.velocity.z);
        _matchRigidbody.AddForce(Vector3.up * _jumpPower, ForceMode.Impulse);
        StartCoroutine(EnabledRaycastsAfterHalfSecond());
    }

    private IEnumerator EnabledRaycastsAfterHalfSecond()
    {
        yield return new WaitForSeconds(0.5f);
    }

    private void CheckJumpInput()
    {
        if (!CanBeControlledByPlayer)
            return;

        if (LevelController.Instance.CurrentMatch != null && Vector3.Distance(transform.position, LevelController.Instance.CurrentMatch.transform.position) > 20f)
            return;

        if (_receiveInput && (Input.GetKeyDown(KeyCode.Space) || _mobileInputVariable.WantsToJump))
        {
            if (_isGrounded)
                Jump();
            else if (_secondJumpPossible)
                Jump();
        }
    }


    private void ClampYVelocity()
    {
        float yVelocity = Mathf.Clamp(_matchRigidbody.velocity.y, _jumpPower * -4, _jumpPower);
        _matchRigidbody.velocity = new Vector3(_matchRigidbody.velocity.x, yVelocity, _matchRigidbody.velocity.z);

        if (_matchRigidbody.velocity.magnitude >= 15 && IsControlledByPlayer)
        {
            Extinguish();
        }
    }

    private void GroundRaycast()
    {
        RaycastHit hit;
        bool hitDetect = Physics.BoxCast(transform.position + Vector3.up * 0.15f, new Vector3(0.2f, 0.1f, 0.2f), transform.up * -1, out hit, transform.rotation, 0.19f, _groundRaycastLayerMask, QueryTriggerInteraction.Ignore);
        if (hitDetect)
        {
            _isGrounded = true;
            _isInAir = false;
            _secondJumpPossible = true;
        }
        else
        {
            _isGrounded = false;
            _isInAir = true;
        }



        //int hitsCount = 0;
        //RaycastHit firstHit;
        //if (Physics.Raycast(transform.position + Vector3.up * 0.15f + Vector3.right * 0.15f, Vector3.down, out firstHit, 0.18f, _groundRaycastLayerMask, QueryTriggerInteraction.Ignore))
        //{
        //    hitsCount++;
        //}

        //RaycastHit secondHit;
        //if (Physics.Raycast(transform.position + Vector3.up * 0.15f + Vector3.right * -0.15f, Vector3.down, out secondHit, 0.18f, _groundRaycastLayerMask, QueryTriggerInteraction.Ignore))
        //{
        //    hitsCount++;
        //}

        //if (hitsCount > 0)
        //{
        //    _isGrounded = true;
        //    _isInAir = false;
        //    _secondJumpPossible = true;
        //}
        //else
        //{
        //    _isGrounded = false;
        //    _isInAir = true;
        //}
    }
}
