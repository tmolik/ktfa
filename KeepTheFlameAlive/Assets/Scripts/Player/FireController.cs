﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class FireController : MonoBehaviour
{
    [System.NonSerialized]
    public bool IsActive = false;

    [SerializeField]
    private UnityEvent OnFireEnded = new UnityEvent();

    [SerializeField]
    private UnityEvent OnFireStarted = new UnityEvent();

    [SerializeField]
    private ParticleSystem _fireParticleSystem = null;

    [SerializeField]
    private Transform _fireTransform = null;

    [SerializeField]
    private Light _fireLight = null;

    [SerializeField]
    private float _fireDuration = 3f;

    [SerializeField]
    private Transform _fireStartTransform = null;

    [SerializeField]
    private Transform _fireEndTransform = null;

    private float _burningTime = 0f;

    private Vector3 previousFramePosition;

    private ParticleSystem.ForceOverLifetimeModule _fireForceOverLifetimeModule;

    public void Start()
    {
        _fireForceOverLifetimeModule = _fireParticleSystem.forceOverLifetime;
    }

    public void Ignite(UnityAction onFireEnded = null)
    {
        _fireParticleSystem.Play();
        _burningTime = 0;
        _fireLight.enabled = true;

        StartCoroutine(InvokeOnFireStarted());

        if (onFireEnded != null)
            OnFireEnded.AddListener(onFireEnded);

        IsActive = true;
    }

    private IEnumerator InvokeOnFireStarted()
    {
        yield return new WaitForEndOfFrame();
        if (OnFireStarted != null)
            OnFireStarted.Invoke();
    }

    public void Update()
    {
        if (IsActive)
        {
            CalculateAndSetFirePosition();
        }
    }

    private void CalculateAndSetFirePosition()
    {
        Vector3 velocity = transform.position - previousFramePosition;
        _fireForceOverLifetimeModule.x = velocity.x * -5;
        _fireForceOverLifetimeModule.y = 0.1f + velocity.y * -5;

        _burningTime += Time.deltaTime;

        if (_burningTime >= _fireDuration)
        {
            StopBurning();
        }

        if (_fireStartTransform == null || _fireEndTransform == null)
            return;

        _fireTransform.position = Vector3.Lerp(_fireStartTransform.position, _fireEndTransform.position, _burningTime / _fireDuration);
        previousFramePosition = transform.position;
    } 

    public void StopBurning()
    {
        _fireParticleSystem.Stop();
        _fireLight.enabled = false;
        OnFireEnded.Invoke();

        IsActive = false;
    }

    public void Extinguish()
    {
        StopBurning();
        _fireParticleSystem.Clear();
    }
}
