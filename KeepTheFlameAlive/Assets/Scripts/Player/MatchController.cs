﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class MatchController : MonoBehaviour
{
    public AudioListener CurrentAudioListener;

    public Vector3 AdditionalVelocityFromMovingObjects = Vector3.zero;

    public bool IsControlledByPlayer = false;

    public bool CanBeControlledByPlayer = true;

    [SerializeField]
    private FireController _fireController = null;

    [SerializeField]
    private Rigidbody _matchRigidbody = null;

    [SerializeField]
    private float _movementSpeed = 2f;

    [SerializeField]
    private float _jumpPower = 5f;

    [SerializeField]
    private LayerMask _groundRaycastLayerMask = 0;

    [SerializeField]
    private AudioSource _igniteAudioSource = null;
    [SerializeField]
    private AudioSource _extuinguishAudioSource = null;

    [Header("Trigger settings")]
    [SerializeField]
    private BoxCollider _collider=null;
    [SerializeField]
    private Vector3 _colliderSizeWhenActive = new Vector3(1, 2, 1);
    [SerializeField]
    private Vector3 _colliderSizeWhenNotActive = new Vector3(3, 2, 3);

    [SerializeField]
    private Collider _frictionlessCollider = null;

    private AudioListener _audioListener;

    private bool _isGrounded = true;
    private bool _isInAir = false;
    private bool _secondJumpPossible = true;

    private bool _receiveInput = true;
    private bool _wasControlledByPlayer = false;

    private void Awake()
    {
        if (CanBeControlledByPlayer)
            _audioListener = GetComponent<AudioListener>();
    }

    private void Start()
    {
        _collider.size = _colliderSizeWhenNotActive;

        if (IsControlledByPlayer)
        {
            TakeControl();
        }
    }

    internal void Extinguish()
    {
        if (_fireController.IsActive)
        {
            _fireController.Extinguish();
            _extuinguishAudioSource.Play();
        }
    }


    public bool CanBeTaken()
    {
        return !_wasControlledByPlayer && CanBeControlledByPlayer;
    }

    public void TakeControl()
    {
        IsControlledByPlayer = true;
        Ignite();
        FollowingCamera.Instance.SetNewTarget(transform);
        LevelController.Instance.CurrentMatch = this;

        _collider.size = _colliderSizeWhenActive;
        _frictionlessCollider.enabled = true;
        if (_audioListener && CanBeControlledByPlayer)
        {
            if (CurrentAudioListener != null)
                CurrentAudioListener.enabled = false;
            _audioListener.enabled = true;
            CurrentAudioListener = _audioListener;
        }
        _wasControlledByPlayer = true;
    }

    public void Ignite()
    {
        if (_fireController.IsActive || _wasControlledByPlayer)
            return;
        _fireController.Ignite(FireEnded);
        _igniteAudioSource.Play();
    }

    public void StopTakingControl()
    {
        IsControlledByPlayer = false;
        _collider.size = _colliderSizeWhenActive;
        _frictionlessCollider.enabled = false;
        EnableRigidbody(true);
        if (LevelController.Instance.CurrentMatch == this)
        {
            LevelController.Instance.CurrentMatch = null;
            LevelController.Instance.CheckIfShouldEndGame();
        }
    }

    public void FireEnded()
    {
        EnableRigidbody(true);
        _matchRigidbody.AddForceAtPosition(UnityEngine.Random.insideUnitSphere * 50f, transform.position + Vector3.up * 1.5f);
        if (LevelController.Instance.CurrentMatch == this)
        {
            LevelController.Instance.CurrentMatch = null;
            LevelController.Instance.CheckIfShouldEndGame();
        }
    }

    public void EnableRigidbody(bool enable)
    {
        if (enable)
        {
            RigidbodyConstraints constraints = _matchRigidbody.constraints;
            constraints = RigidbodyConstraints.None;
            _matchRigidbody.constraints = constraints;
        }
    }

    public void AddForceToRigidbody(Vector3 force)
    {
        _matchRigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void AddForceToRigidbodyAtPosition(Vector3 force, Vector3 position)
    {
        _matchRigidbody.AddForceAtPosition(force, position, ForceMode.Impulse);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!IsControlledByPlayer)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out MatchController matchController))
        {
            if (matchController.CanBeTaken())
            {
                StopTakingControl();
                matchController.TakeControl();
            }
            else
            {
                matchController.Ignite();
            }
        }
    }
}
