﻿using UnityEngine;

public class FollowingCamera : MonoBehaviour
{
    public static FollowingCamera Instance { get; private set; }

    public FollowingCamera()
    {
        Instance = this;
    }

    [SerializeField]
    private Transform _target = null;

    [SerializeField]
    private Vector3 _cameraOffset = Vector3.zero;

    [SerializeField]
    private Vector3 _cameraTargetRotation = Vector3.zero;

    [SerializeField]
    private float _cameraSpeed = 3f;

    private float _shake = 0;
    private float _shakeAmount = 0.75f;
    private float _decreaseFactor = 1.0f;

    // Start is called before the first frame update
    private void Start()
    {
        if (_target != null)
            transform.position = _target.position + _cameraOffset;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_target == null)
            return;

        SetCameraPositionAndRotation();
    }

    private void SetCameraPositionAndRotation()
    {
        Vector3 shakeBonus = Vector3.zero;
        if (_shake > 0)
        {
            shakeBonus = Random.insideUnitSphere * _shakeAmount;
            shakeBonus = new Vector3(shakeBonus.x, 0, shakeBonus.z);
            _shake -= Time.deltaTime * _decreaseFactor;
        }

        transform.position = Vector3.Lerp(transform.position, _target.position + _cameraOffset, Time.deltaTime * _cameraSpeed) + shakeBonus;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(_cameraTargetRotation), Time.deltaTime * 2.5f);
    }

    public void SetNewTarget(Transform newTarget)
    {
        _target = newTarget;
    }

    public void SetNewTargetOffsetAndRotation(Vector3 newOffset, Vector3 newRotation)
    {
        _cameraOffset = newOffset;
        _cameraTargetRotation = newRotation;
    }

    public void CameraShake()
    {
        _shake = .6f;
    }

    [ContextMenu("Go to target instantly")]
    public void GoToTargetInstantly()
    {
        if (_target == null)
            return;

        transform.position = _target.position + _cameraOffset;
        transform.rotation = Quaternion.Euler(_cameraTargetRotation);
    }
}
