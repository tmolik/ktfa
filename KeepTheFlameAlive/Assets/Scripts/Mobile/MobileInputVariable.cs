using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MobileInput", menuName = "ScriptableObjects/MobileInput")]
public class MobileInputVariable : ScriptableObject
{
    public Vector2 MovementVector;
    public bool WantsToJump;
}
