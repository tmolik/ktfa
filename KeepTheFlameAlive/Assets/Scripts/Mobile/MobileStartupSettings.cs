using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileStartupSettings : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
#endif
    }

   
}
