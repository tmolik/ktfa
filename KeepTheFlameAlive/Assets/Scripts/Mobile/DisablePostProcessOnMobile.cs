using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(PostProcessLayer))]
public class DisablePostProcessOnMobile : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_ANDROID
        GetComponent<PostProcessLayer>().enabled = false;
#endif
    }
}
