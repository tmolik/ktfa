﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInputUiController : MonoBehaviour
{
    [SerializeField]
    private MobileInputVariable _mobileInputVariable;

    [SerializeField]
    private RectTransform indicatorRect = null;

    [SerializeField]
    private float maxDistance = 50;



    private RectTransform rectTransform;

    private Vector3 _directionVector;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (indicatorRect == null)
            return;
        _mobileInputVariable.MovementVector = Vector3.zero;


#if UNITY_EDITOR
        EditorUpdate();
#elif UNITY_ANDROID
        AndroidUpdate();
#endif
    }

    private void EditorUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            float distance = Vector3.Distance(Input.mousePosition, rectTransform.position);
            if (distance > maxDistance)
            {
                Vector3 direction = (Input.mousePosition - rectTransform.position).normalized;
                indicatorRect.position = rectTransform.position + direction * maxDistance;
                Debug.Log("rect transform position " + rectTransform.position + " direction " + direction + " maxDistance " + maxDistance + " indicator rect position " + indicatorRect.position);

            }
            else
            {
                indicatorRect.position = Input.mousePosition;
                Debug.Log("rect transform position " + rectTransform.position + " indicator rect " + indicatorRect);
            }

            CheckAngle(distance);
        }
        else
        {
            _directionVector = Vector3.zero;
            indicatorRect.position = rectTransform.position;

        }
    }

    private void AndroidUpdate()
    {
        if (Input.touchCount == 0)
            return;

        _mobileInputVariable.WantsToJump = false;
        for (int i = 0; i <= Input.touchCount - 1; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                float distance = Vector3.Distance(touch.position, rectTransform.position);
                if (distance > maxDistance)
                {
                    Vector3 inputVector3 = new Vector3(touch.position.x, touch.position.y, 0);
                    Vector3 direction = (inputVector3 - rectTransform.position).normalized;
                    indicatorRect.position = rectTransform.position + direction * maxDistance;
                }
                else
                {
                    Vector3 inputVector3 = new Vector3(touch.position.x, touch.position.y, 0);
                    indicatorRect.position = inputVector3;
                }

                CheckAngle(distance);
            }
            else
            {
                _directionVector = Vector3.zero;
                indicatorRect.position = rectTransform.position;

                if (touch.phase == TouchPhase.Began)
                {
                    if (touch.position.x > Screen.width / 2)
                    {
                        //Jump
                        _mobileInputVariable.WantsToJump = true;
                    }
                }
            }
        }
    }

    private void CheckAngle(float distance)
    {
        distance = Mathf.Clamp(distance, 0, maxDistance);

        Vector3 direction = (Input.mousePosition - rectTransform.position).normalized;
        _directionVector = direction * distance/maxDistance;
        _mobileInputVariable.MovementVector = _directionVector;
        return;

        //float angleRight = Vector3.Angle(Vector3.right, direction);
        //if (angleRight > 0 && angleRight < 45)
        //{
        //    _directionVector = Vector3.right;
        //}
        //else if (angleRight > 135 && angleRight < 180)
        //{
        //    _directionVector = Vector3.left;
        //}

        //float angleUp = Vector3.Angle(Vector3.up, direction);
        //if (angleUp > 0 && angleUp < 45)
        //{
        //    _directionVector = Vector3.forward;
        //}
        //else if (angleUp > 135 && angleUp < 180)
        //{
        //    _directionVector = Vector3.back;
        //}
    }
}
