﻿[System.Serializable]
public class GameData
{
    public int CurrentLevel = -1;
    public int CurrentCheckpoint = -1;
}
