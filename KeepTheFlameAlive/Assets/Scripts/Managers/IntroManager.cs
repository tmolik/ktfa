﻿using UnityEngine;

public class IntroManager : TimelineManager
{
    [SerializeField]
    protected bool _debugSkipIntro = false;

    [SerializeField]
    private CanvasGroup _blackCanvasGroup = null;

    [SerializeField]
    private MatchController _startingMatch = null;

    private void Start()
    {
        if (CheckpointsController.Instance.GetCurrentCheckpointId() < 0 && !_debugSkipIntro)
        {
            StartTimeline();
        }
        else
        {
            _blackCanvasGroup.alpha = 0;
            _startingMatch.gameObject.SetActive(true);
            _startingMatch.TakeControl();
        }
    }

}

