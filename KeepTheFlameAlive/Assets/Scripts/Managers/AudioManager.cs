﻿using ObjectPooling;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField]
    private AudioMixerGroup _masterMixerGroup = null;

    [SerializeField]
    private AudioMixerGroup _musicMixerGroup = null;

    public override void Awake()
    {
        base.Awake();
        SettingsManager.OnSettingsLoaded += SetupVolume;
    }

    private void OnDestroy()
    {
        SettingsManager.OnSettingsLoaded += SetupVolume;
    }

    private void SetupVolume()
    {
        SetMasterVolume();
        SetMusicVolume();
    }

    public void SetMasterVolume()
    {
        _masterMixerGroup.audioMixer.SetFloat("MasterVolume", Mathf.Log(SettingsManager.CurrentSettings.MasterVolume) * 20);
    }

    public void SetMusicVolume()
    {
        _musicMixerGroup.audioMixer.SetFloat("MusicVolume", Mathf.Log(SettingsManager.CurrentSettings.MusicVolume) * 20);
    }

    public void PlayAudioAtPosition(AudioClip clip, Vector3 position, float maxDistance)
    {
        AudioSource audioSource = ObjectPooler.Get.GetPooledObject(EPoolables.AudioSource2D).GetComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.transform.position = position;
        audioSource.gameObject.SetActive(true);
        audioSource.spatialBlend = 1;
        audioSource.maxDistance = maxDistance;
        audioSource.Play();
    }

    public void PlayAudio2D(AudioClip clip)
    {
        AudioSource audioSource = ObjectPooler.Get.GetPooledObject(EPoolables.AudioSource2D).GetComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.gameObject.SetActive(true);
        audioSource.spatialBlend = 0;
        audioSource.Play();
    }
}
