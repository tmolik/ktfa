﻿using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    [SerializeField]
    private List<DialogHelper> _dialogs = new List<DialogHelper>();

    [SerializeField]
    private Camera _introCamera = null;

    [SerializeField]
    protected PlayableDirector _introDirector = null;

    private int _currentDialogIndexToShow;

    public void StartTimeline()
    {
        _introDirector.Play();
    }

    public void ShowDialog()
    {
        if (_currentDialogIndexToShow >= _dialogs.Count)
            return;
        DialogHelper dialogToShow = _dialogs[_currentDialogIndexToShow];
        UiSayingsManager.Instance.CreateAndSetupIntroSaying(_introCamera, dialogToShow.SpeakerTarget, dialogToShow.Offset, dialogToShow.DialogText, dialogToShow.Duration);
        _currentDialogIndexToShow++;
    }
}
