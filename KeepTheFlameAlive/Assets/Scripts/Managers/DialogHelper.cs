﻿using UnityEngine;

[System.Serializable]
public class DialogHelper
{
    public string DialogText;
    public Transform SpeakerTarget;
    public Vector3 Offset;
    public float Duration;
}