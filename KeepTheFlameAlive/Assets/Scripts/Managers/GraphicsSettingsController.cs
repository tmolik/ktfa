﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class GraphicsSettingsController : Singleton<GraphicsSettingsController>
{
    [SerializeField]
    private PostProcessProfile _gameplayPostProcessProfile = null;

    public override void Awake()
    {
        base.Awake();
        SettingsManager.OnSettingsLoaded += SetupBrightness;
        QualitySettings.SetQualityLevel(5);
    }

    public void SetupBrightness()
    {
        ColorGrading colorGradingLayer = null;
        _gameplayPostProcessProfile.TryGetSettings(out colorGradingLayer);
        colorGradingLayer.gamma.value = new Vector4(1.0f,0.9f,0.8f, SettingsManager.CurrentSettings.Brightness);
    }
}
