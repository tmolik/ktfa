﻿using Molioo.Localization;

[System.Serializable]
public class SettingsData
{
    public ELanguage Language;

    public float MasterVolume = 1;

    public float MusicVolume = 1;

    public float Brightness =0f;
}
