﻿using UnityEngine;

public class CursorManager : Singleton<CursorManager>
{
    public void ShowCursor()
    {
        Cursor.visible = true;
    }

    public void HideCursor()
    {
        Cursor.visible = false;
    }
}
