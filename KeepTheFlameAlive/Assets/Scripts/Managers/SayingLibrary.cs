﻿using System.Collections.Generic;
using UI;
using UnityEngine;

public class SayingLibrary : Singleton<SayingLibrary>
{
    [SerializeField]
    private TextAsset _beginingSayingsTextAsset = null;

    [SerializeField]
    private TextAsset _endingSayingsTextAsset = null;

    private List<string> _beginingSayings = new List<string>();
    private List<string> _endingSayings = new List<string>();

    public override void Awake()
    {
        base.Awake();
        if (_beginingSayingsTextAsset != null)
        {
            foreach(string beginingSaying in _beginingSayingsTextAsset.text.Split('\n'))
            {
                _beginingSayings.Add(beginingSaying.Trim());
            }
            //Delete the last one as its always empty
            _beginingSayings.RemoveAt(_beginingSayings.Count - 1);
        }
        if (_endingSayingsTextAsset != null)
        {
            foreach (string endingSaying in _endingSayingsTextAsset.text.Split('\n'))
            {
                _endingSayings.Add(endingSaying.Trim());
            }
            //Delete the last one as its always empty
            _endingSayings.RemoveAt(_endingSayings.Count - 1);
        }
    }


    public string GetRandomSaying(ESayingType sayingType)
    {
        if (sayingType == ESayingType.AtTheBeginning)
            return _beginingSayings.GetRandom();
        else
            return _endingSayings.GetRandom();
    }
}
