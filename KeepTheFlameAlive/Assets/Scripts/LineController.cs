﻿using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour
{
    [SerializeField]
    private LineRenderer _lineRenderer = null;

    [SerializeField]
    public List<Transform> _linePoints = new List<Transform>();

    private Transform _lastTransform;

    private Vector3 _previousFrameLastTransformPosition;

    public void OnEnable()
    {
        _lastTransform = _linePoints.Last();
        _previousFrameLastTransformPosition = _lastTransform.position;
    }

    private void OnValidate()
    {
        UpdateRenderingPositions();
    }

    public void Update()
    {
        CalculateNewPositions();
        UpdateRenderingPositions();
    }

    private void CalculateNewPositions()
    {
        Vector3 diff = _lastTransform.position - _previousFrameLastTransformPosition;

        float diffMagnitude = diff.magnitude;
        if (diffMagnitude != 0)
        {
            //Move position 0 towards position 1 by this magnitude

            Vector3 direction = (_linePoints[1].position - _linePoints[0].position).normalized;
            _linePoints[0].position += direction * diffMagnitude;

            if (Vector3.Distance(_linePoints[0].position, _linePoints[1].position) < 0.4f && _linePoints.Count > 2)
                RemoveFirstPoint();
        }
        _previousFrameLastTransformPosition = _lastTransform.position;
    }

    private void RemoveFirstPoint()
    {
        _linePoints.RemoveAt(0);
    }

    private void UpdateRenderingPositions()
    {
        if (_lineRenderer != null && _linePoints.Count > 0)
        {
            _lineRenderer.positionCount = _linePoints.Count;
            for (int i = 0; i < _linePoints.Count; i++)
            {
                _lineRenderer.SetPosition(i, _linePoints[i].position);
            }
        }
    }


}
