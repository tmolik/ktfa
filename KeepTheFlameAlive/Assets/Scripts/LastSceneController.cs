﻿using UnityEngine;

public class LastSceneController : MonoBehaviour
{
    public void FinishGame()
    {
        GameFilesManager.CurrentGameData.CurrentLevel = -1;
        GameFilesManager.CurrentGameData.CurrentCheckpoint = -1;
        GameFilesManager.SaveProgress();
        LevelsManager.Instance.GoBackToMenu();
    }
}
