﻿namespace ObjectPooling
{
    public enum EPoolables
    {
        WaterDrop,
        UiSaying,
        WaterDropSplash,
        AudioSource2D,
        WaterDropSound
    }
}